import { Component, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/services/recipe.service';
import { RecipeCardData } from 'src/app/types/recipe-card-data';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  recipeCards: Array<RecipeCardData> = [];

  page: number = 1;
  searchQuery: String = "";
  isLoading = false;

  constructor(
    public recipeService: RecipeService,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loadRecipes();
  }

  searchUpdate(): void {
    this.loadRecipes();
  }

  private async loadRecipes(): Promise<void> {
    this.page = 1;
    this.isLoading = true;
    this.recipeCards = await this.recipeService.getPublicRecipeList(this.page, this.searchQuery);
    this.isLoading = false;
  }

  /**
   * Load next recipes in infinity scroll
   * @param event 
   */
  async loadNext(event): Promise<void> {
    this.page += 1;
    this.isLoading = true;
    let newRecipes = await this.recipeService.getPublicRecipeList(this.page, this.searchQuery);
    newRecipes.forEach(r => this.recipeCards.push(r));
    this.isLoading = false;
    event.target.complete();
  }

}
