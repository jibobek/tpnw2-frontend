import { Component, OnInit } from '@angular/core';
import { AppPage } from 'src/app/enums/app-page';
import { RouterService } from 'src/app/services/router.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm = {
    email: "",
    password: ""
  };
  showInputs = false;
  isRegister = false;

  constructor(
    private userService: UserService,
    private routerService: RouterService,
  ) { }

  ngOnInit() { }

  login(): void {
    this.showInputs = true;

  }

  register(): void {
    this.showInputs = true;
    this.isRegister = true;

  }

  async complete(): Promise<void> {
    let status: Boolean | null = null;
    if (this.isRegister) {
      status = await this.userService.register(this.loginForm.email, this.loginForm.password);
    } else {
      status = await this.userService.login(this.loginForm.email, this.loginForm.password);
    }

    if (status) {
      this.showInputs = false;
      this.isRegister = false;
      this.loginForm = {
        email: "",
        password: ""
      };
      await this.routerService.goToPage(AppPage.RECIPE_LIST);
    }
  }
}
