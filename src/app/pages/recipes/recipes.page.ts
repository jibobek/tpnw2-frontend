import { Component } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { RouterService } from 'src/app/services/router.service';
import { AppPage } from 'src/app/enums/app-page';

@Component({
  selector: 'app-recipes',
  templateUrl: 'recipes.page.html',
  styleUrls: ['recipes.page.scss']
})
export class RecipesPage {

  constructor(
    public storageService: StorageService,
    private routerService: RouterService,
  ) { }

  goLogin(): void {
    this.routerService.goToPage(AppPage.LOGIN);
  }

  /**
   * Handle logout button
   */
  async logout(): Promise<void> {
    await this.storageService.clearStorage();
    this.routerService.goToPage(AppPage.RECIPE_LIST);
    window.location.reload();
  }
}
