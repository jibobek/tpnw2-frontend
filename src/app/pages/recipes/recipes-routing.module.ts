import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesPage } from './recipes.page';

const routes: Routes = [
  {
    path: 'recipes',
    component: RecipesPage,
    children: [
      {
        path: 'list',
        loadChildren: () => import('../list/list.module').then(m => m.ListPageModule)
      },
      {
        path: 'my',
        loadChildren: () => import('../my/my.module').then(m => m.MyPageModule)
      },
      {
        path: 'new',
        loadChildren: () => import('../new/new.module').then(m => m.NewPageModule)
      },
      {
        path: 'new/:id',
        loadChildren: () => import('../new/new.module').then(m => m.NewPageModule)
      },
      {
        path: 'view/:id',
        loadChildren: () => import('../view/view.module').then(m => m.ViewPageModule)
      },
      {
        path: '',
        redirectTo: '/recipes/list',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/recipes/list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class RecipesPageRoutingModule {}
