import { Component, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/services/recipe.service';
import { RecipeCardData } from 'src/app/types/recipe-card-data';

@Component({
  selector: 'app-my',
  templateUrl: './my.page.html',
  styleUrls: ['./my.page.scss'],
})
export class MyPage implements OnInit {

  recipeCards: Array<RecipeCardData> = [];

  page: number = 1;
  isLoading = false;

  constructor(
    public recipeService: RecipeService,
  ) { }

  ngOnInit() {
  }


  ionViewWillEnter() {
    this.loadRecipes();
  }

  private async loadRecipes(): Promise<void> {
    this.page = 1;
    this.isLoading = true;
    this.recipeCards = await this.recipeService.getMyRecipeList(this.page);
    this.isLoading = false;
  }

  /**
   * Load next recipes in infinity scroll
   * @param event 
   */
  async loadNext(event): Promise<void> {
    this.page += 1;
    this.isLoading = true;
    let newRecipes = await this.recipeService.getMyRecipeList(this.page);
    newRecipes.forEach(r => this.recipeCards.push(r));
    this.isLoading = false;
    event.target.complete();
  }

}
