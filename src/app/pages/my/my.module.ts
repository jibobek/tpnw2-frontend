import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyPageRoutingModule } from './my-routing.module';

import { MyPage } from './my.page';
import { RecipeListComponentModule } from 'src/app/components/recipe-list/recipe-list.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyPageRoutingModule,
    RecipeListComponentModule
  ],
  declarations: [MyPage]
})
export class MyPageModule {}
