import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppPage } from 'src/app/enums/app-page';
import { RecipeService } from 'src/app/services/recipe.service';
import { RouterService } from 'src/app/services/router.service';
import { Comment } from 'src/app/types/comment';
import { Recipe } from 'src/app/types/recipe';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.page.html',
  styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {

  public recipeId: String = "";
  public recipe: Recipe = {
    id: "",
    name: "",
    description: "",
    rating: 0,
    isPrivate: false,
    images: [],
    steps: [],
    ingredients: [],
    own: false
  }
  public comments: Array<Comment> = [];
  public isLoading: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private routerService: RouterService,
    private recipeService: RecipeService,
    private alertController: AlertController,
    private toastController: ToastController,
    public storageService: StorageService,
  ) { }

  ngOnInit() {
    /**
     * Get recipe id from url
     */
    this.recipeId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  async ionViewDidEnter() {
    if (this.recipeId !== "") {
      await this.loadRecipe();
      this.loadComments();
    } else {
      this.routerService.goToPage(AppPage.RECIPE_LIST);
    }
  }

  private async loadRecipe(): Promise<void> {
    this.isLoading = true;
    this.recipe = await this.recipeService.getRecipe(this.recipeId);
    this.isLoading = false;
    return;
  }

  private async loadComments(): Promise<void> {
    this.isLoading = true;
    this.comments = await this.recipeService.getRecipeComments(this.recipeId);
    console.log("load recipe comments", this.comments);
    this.isLoading = false;
    return;
  }

  public async addComment() {
    const alert = await this.alertController.create({
      header: 'Přidání komentáře',
      inputs: [{ name: 'text', type: 'text', placeholder: 'Komentář…' }],
      buttons: [{ text: 'Zrušit', role: 'cancel' }, {
        text: 'Přidat',
        handler: async (data) => {
          const state = await this.recipeService.addComment(this.recipeId, data.text);
          if (state) {
            this.loadComments();
            this.showAddedComment();
          } else {
            this.showBadCommentAler();
          }
        }
      }]
    });
    await alert.present();
  }

  private async showBadCommentAler(): Promise<void> {
    const alert = await this.alertController.create({ header: 'Chyba!', message: 'Komentář je moc krátký nebo dlouhý.', buttons: ['OK'] });
    await alert.present();
  }

  public async addRating() {
    const alert = await this.alertController.create({
      header: 'Přidání hodnocení',
      inputs: [
        { name: 'rating1', type: 'radio', label: '5 hvězdiček', value: '5' },
        { name: 'rating2', type: 'radio', label: '4 hvězdičky', value: '4' },
        { name: 'rating3', type: 'radio', label: '3 hvězdičky', value: '3' },
        { name: 'rating4', type: 'radio', label: '2 hvězdičky', value: '2' },
        { name: 'rating5', type: 'radio', label: '1 hvězdičeka', value: '1' }
      ],
      buttons: [
        { text: 'Zrušit', role: 'cancel' },
        {
          text: 'Přidat',
          handler: async (data) => {
            const state = await this.recipeService.addRating(this.recipeId, +data);
            if (state) {
              this.loadRecipe();
              this.showAddedRating();
            } else {
              this.showAlreadyAddedRating();
            }
          }
        }
      ]
    });
    await alert.present();
  }

  private async showAddedComment(): Promise<void> {
    const toast = await this.toastController.create({
      message: 'Komentář přidán.', duration: 2000
    });
    toast.present();
  }

  private async showAddedRating(): Promise<void> {
    const toast = await this.toastController.create({
      message: 'Hodnocení přidáno.', duration: 2000
    });
    toast.present();
  }

  private async showAlreadyAddedRating(): Promise<void> {
    const toast = await this.toastController.create({
      message: 'Recept jste již hodnotil.', duration: 2000
    });
    toast.present();
  }


  public async deleteRecipeAlert(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Smazání receptu',
      message: `Opravdu si přejete smazat recept ${this.recipe.name}?`,
      buttons: [{
        text: 'Zrušit'
      },
      {
        text: 'Ano',
        handler: () => this.deleteRecipe()
      }]
    });

    return await alert.present();
  }

  private async deleteRecipe(): Promise<void> {
    await this.recipeService.deleteRecipe(this.recipe);
    this.routerService.goToPage(AppPage.RECIPE_MY_LIST);
    const toast = await this.toastController.create({
      message: 'Recept smazán.', duration: 2000
    });
    toast.present();
  }
}
