import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewPageRoutingModule } from './view-routing.module';

import { ViewPage } from './view.page';
import { StarRatingComponentModule } from '../../components/star-rating/star-rating.module';
import { IngredientViewComponentModule } from 'src/app/components/ingredient-view/ingredient-view.module';
import { ImageSlidesComponentModule } from 'src/app/components/image-slides/image-slides.module';
import { RecipeStepComponentModule } from 'src/app/components/recipe-step/recipe-step.module';
import { CommentComponentModule } from 'src/app/components/comment/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewPageRoutingModule,
    StarRatingComponentModule,
    IngredientViewComponentModule,
    ImageSlidesComponentModule,
    RecipeStepComponentModule,
    CommentComponentModule,
  ],
  declarations: [ViewPage]
})
export class ViewPageModule { }
