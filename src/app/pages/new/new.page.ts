import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/app/types/recipe';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { RecipeService } from 'src/app/services/recipe.service';
import { RouterService } from 'src/app/services/router.service';
import { AppPage } from 'src/app/enums/app-page';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.page.html',
  styleUrls: ['./new.page.scss'],
})
export class NewPage implements OnInit {

  recipe: Recipe = {
    id: "",
    name: "",
    description: "",
    rating: 0,
    isPrivate: false,
    images: [],
    steps: [],
    ingredients: [],
    own: false
  };
  editRecipeId: String;

  constructor(
    private toastController: ToastController,
    private alertController: AlertController,
    private recipeService: RecipeService,
    private routerService: RouterService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.editRecipeId = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.editRecipeId !== null) {
      this.loadRecipeToEdit();
    }
  }

  private async loadRecipeToEdit(): Promise<void> {
    this.recipe = await this.recipeService.getRecipe(this.editRecipeId);
  }

  addStep(): void {
    this.recipe.steps.push("");
  }

  removeStep(index: number): void {
    this.recipe.steps.splice(index, 1);
  }

  addIngredient(): void {
    this.recipe.ingredients.push({
      quantity: 1,
      unit: "",
      name: ""
    });
  }

  removeIngredient(index: number): void {
    this.recipe.ingredients.splice(index, 1);
  }

  removeImage(index: number): void {
    this.recipe.images.splice(index, 1);
  }

  /**
   * Fix input 2 way binding in ngFor
   * @param index 
   * @param obj 
   * @returns number
   */
  trackByIdx(index: number, obj: any): number {
    return index;
  }

  /**
   * Validate image extension, add to recipe images
   * @param event 
   * @returns void
   */
  public async handleFileInput(event) {
    const file = event.target.files[0];
    if (!file.name.match(/.(jpg|jpeg|png|gif|webp)$/i)) {
      const alert = await this.alertController.create({ header: 'Chyba!', message: 'Soubor není validní obrázek.', buttons: ['OK'] });
      await alert.present();
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.recipe.images.push(reader.result.toString());
      event.path[0].value = "";
      this.showFileAdded();
    };
  }

  private async showFileAdded(): Promise<void> {
    const toast = await this.toastController.create({
      message: 'Fotografie přidána.', duration: 2000
    });
    toast.present();
  }

  private async showRecipeAdded(): Promise<void> {
    const toast = await this.toastController.create({
      message: 'Recept uložen.', duration: 2000
    });
    toast.present();
  }

  private async showBadRecipe(): Promise<void> {
    const alert = await this.alertController.create({ header: 'Chyba!', message: 'Recept se nepodařilo přidat. Zkontrolujte, zda je název a popis v pořádku (délka, vyplnění). Suroviny jsou povinné.', buttons: ['OK'] });
    await alert.present();
  }

  async saveRecipe() {
    console.log("save recipe", this.recipe);
    if (this.editRecipeId === null) {
      let state = await this.recipeService.createRecipe(this.recipe);
      if (state) {
        this.showRecipeAdded();
        this.routerService.goToPage(AppPage.RECIPE_MY_LIST);
        return;
      }
      this.showBadRecipe();
    } else {
      let state = await this.recipeService.updateRecipe(this.recipe);
      if (state) {
        this.showRecipeAdded();
        this.routerService.goToPage(AppPage.RECIPE_MY_LIST);
        return;
      }
      this.showBadRecipe();
    }
  }
}
