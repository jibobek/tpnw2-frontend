export interface RecipeItem {
    id: String,
    name: String,
    rating: Number,
    isPrivate: boolean,
    images: Array<String>,
}
