export interface RecipeCardData {
    id: String,
    name: String,
    stars: Number,
    isPrivate: boolean,
    imageSrc: String,
}
