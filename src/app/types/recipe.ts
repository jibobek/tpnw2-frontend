import { Ingredient } from "./ingredient";

export interface Recipe {
    id: String,
    name: String,
    description: String,
    rating: Number,
    isPrivate: boolean,
    images: Array<String>,
    steps: Array<String>,
    ingredients: Array<Ingredient>,
    own: boolean
}
