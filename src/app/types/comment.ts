export interface Comment {
    id: String,
    userId: String,
    date: String,
    text: String,
}
