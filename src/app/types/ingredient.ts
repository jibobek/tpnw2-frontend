export interface Ingredient {
    quantity: Number,
    unit: String,
    name: String,
}
