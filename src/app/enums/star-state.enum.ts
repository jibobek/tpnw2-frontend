export enum StarState {
    FULL = "star",
    HALF = "star-half",
    EMPTY = "star-outline"
};