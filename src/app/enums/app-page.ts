export enum AppPage {
    LOGIN = "/login",
    REGISTER = "/register",
    RECIPE_LIST = "/recipes/list",
    RECIPE_MY_LIST = "/recipes/my",
    RECIPE_NEW = "/recipes/new",
    RECIPE_VIEW = "/recipes/view"
};