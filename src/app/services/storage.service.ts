import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private authenticationHash: String = "";
  private userEmail: String = "";

  constructor(
    private storage: Storage,
  ) { }

  public getAuthenticationHash(): String {
    return this.authenticationHash;
  }

  public setAuthenticationHash(authenticationHash: string): void {
    this.authenticationHash = authenticationHash;
    this.saveUserToStorage();
  }

  public setUserEmail(userEmail: String): void {
    this.userEmail = userEmail;
    this.saveUserToStorage();
  }

  public getUserEmail(): String {
    return this.userEmail;
  }

  /**
   * Save auth token and user email from storage
   * @returns Promise
   */
  private async saveUserToStorage(): Promise<void> {
    await this.storage.set("user", {
      authenticationHash: this.authenticationHash,
      userEmail: this.userEmail
    });
    return;
  }

  /**
   * Load auth token and user email from storage
   * @returns Promise
   */
  public async loadUserFromStorage(): Promise<void> {
    const data = await this.storage.get("user");
    if (data !== null) {
      this.authenticationHash = data.authenticationHash;
      this.userEmail = data.userEmail;
    }
    return;
  }

  public initStorage(): Promise<Storage> {
    return this.storage.create();
  }

  public async clearStorage(): Promise<void> {
    await this.storage.clear();
    return;
  }
}
