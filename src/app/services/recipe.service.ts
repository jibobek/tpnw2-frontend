import { Injectable } from '@angular/core';
import { AppPage } from '../enums/app-page';
import { Comment } from '../types/comment';
import { Ingredient } from '../types/ingredient';
import { Recipe } from '../types/recipe';
import { RecipeCardData } from '../types/recipe-card-data';
import { RecipeItem } from '../types/recipe-list-item';
import { EndpointService } from './endpoint.service';
import { RouterService } from './router.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor(
    private endPointService: EndpointService,
    private routerService: RouterService,
    private storageService: StorageService,
  ) { }


  public convertApiToRecipeItem(recipeItem: any): RecipeItem {
    return {
      id: recipeItem._id,
      name: recipeItem.name,
      rating: +recipeItem.rating,
      isPrivate: recipeItem.isPrivate,
      images: recipeItem.images,
    } as RecipeItem;
  }

  public convertRecipeItemToCarddata(recipeItem: RecipeItem): RecipeCardData {
    return {
      id: recipeItem.id,
      name: recipeItem.name,
      stars: recipeItem.rating,
      isPrivate: recipeItem.isPrivate,
      imageSrc: recipeItem.images[0] || "",
    };
  }

  public convertApiToRecipeCardData(recipeItemList: Array<any>): Array<RecipeCardData> {
    let items = [];
    recipeItemList.forEach(r => {
      const recipeItem: RecipeItem = this.convertApiToRecipeItem(r);
      const recipeCardData: RecipeCardData = this.convertRecipeItemToCarddata(recipeItem);
      items.push(recipeCardData);
    });
    return items;
  }

  /**
   * Convert plain object to Recipe
   * @param recipe 
   * @returns Recipe
   */
  public convertApiToRecipe(recipe: any): Recipe {
    let ingredients: Array<Ingredient> = [];
    recipe.ingredients.forEach(i => ingredients.push(this.convertApiToIngredient(i)));
    return {
      id: recipe._id,
      name: recipe.name,
      description: recipe.description,
      rating: recipe.rating,
      isPrivate: recipe.isPrivate,
      images: recipe.images,
      steps: recipe.steps,
      ingredients: ingredients,
      own: recipe.userEmail === this.storageService.getUserEmail()
    } as Recipe;
  }

  public convertApiToComment(comment: any): Comment {
    return {
      id: comment._id,
      userId: comment.userId,
      date: comment.date,
      text: comment.text,
    } as Comment;
  }

  public convertApiToComments(comments: Array<any>): Array<Comment> {
    let commentList: Array<Comment> = [];
    comments.forEach(c => commentList.push(this.convertApiToComment(c)));
    return commentList;
  }

  public async getPublicRecipeList(page: Number = 1, searchQuery: String = ""): Promise<Array<RecipeCardData>> {
    const response = await this.endPointService.getPublicRecipeList(page, searchQuery);
    return this.convertApiToRecipeCardData(response);
  }

  public async getMyRecipeList(page: Number = 1): Promise<Array<RecipeCardData>> {
    const response = await this.endPointService.getMyRecipes(page);
    return this.convertApiToRecipeCardData(response);
  }

  public convertApiToIngredient(ingredient: any): Ingredient {
    return {
      id: ingredient._id,
      quantity: ingredient.quantity,
      unit: ingredient.unit,
      name: ingredient.name,
    } as Ingredient;
  }

  public async getRecipe(recipeId: String): Promise<Recipe> {
    const response = await this.endPointService.getRecipe(recipeId);
    return this.convertApiToRecipe(response);
  }

  public openRecipe(recipeId: String): Promise<boolean> {
    return this.routerService.goToPageWithId(AppPage.RECIPE_VIEW, recipeId);
  }

  public editRecipe(recipeId: String): Promise<boolean> {
    return this.routerService.goToPageWithId(AppPage.RECIPE_NEW, recipeId);
  }

  public async getRecipeComments(recipeId: String): Promise<Array<Comment>> {
    const response = await this.endPointService.getComments(recipeId);
    return this.convertApiToComments(response);
  }

  public async addComment(recipeId: String, text: String): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.endPointService.createComment(recipeId, text)
        .then(() => resolve(true))
        .catch(err => resolve(false))
    });
  }

  public addRating(recipeId: String, rating: Number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.endPointService.createRating(recipeId, rating)
        .then(() => resolve(true))
        .catch(err => resolve(false))
    });
  }

  public createRecipe(recipe: Recipe): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.endPointService.createRecipe(recipe)
        .then(() => resolve(true))
        .catch(err => resolve(false))
    });
  }

  public deleteRecipe(recipe: Recipe): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.endPointService.deleteRecipe(recipe.id)
        .then(() => resolve(true))
        .catch(err => resolve(false))
    });
  }

  public updateRecipe(recipe: Recipe): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.endPointService.updateRecipe(recipe.id, recipe)
        .then(() => resolve(true))
        .catch(err => resolve(false))
    });
  }
}
