import { Injectable } from '@angular/core';
import { AlertService } from './alert.service';
import { EndpointService } from './endpoint.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private storageService: StorageService,
    private endPointService: EndpointService,
    private alertService: AlertService,
  ) { }


  public async login(email: String, password: String): Promise<Boolean> {
    let state: Boolean | null;
    await this.endPointService.login(email, password)
      .then(response => {
        this.storageService.setUserEmail(email);
        this.storageService.setAuthenticationHash(response.accessToken);
        state = true;
      }).catch(err => {
        this.alertService.show("Chyba!", "Přihlášení se nezdařilo. Zkontrolujte údaje.");
        state = false;
      });
    return state;
  }

  public async register(email: String, password: String): Promise<Boolean> {
    let state: Boolean | null;
    await this.endPointService.register(email, password)
      .then(async response => {
        await this.login(email, password);
        state = true;
      }).catch(err => {
        this.alertService.show("Chyba!", "Registrace se nezdařila. Zkontrolujte údaje.");
        state = false;
      });

    return state;
  }
}
