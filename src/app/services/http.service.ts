import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { AppConfiguration } from '../const/config';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(
    private angularHttp: HttpClient,
    private storageService: StorageService,
  ) { }

  public post(url: string, body: object, authorize: boolean = true): Promise<any> {
    let httpOptions = {
      headers: {
        "Content-Type": "application/json"
      }
    };
    if (authorize) {
      httpOptions.headers["Authorization"] = `Bearer ${this.storageService.getAuthenticationHash()}`;
    }

    console.log('POST request', url, body, httpOptions);
    return this.angularHttp.post(url, JSON.stringify(body), httpOptions).toPromise();
  }

  public get(url: string, data?: object): Promise<any> {
    if (data) {
      url += '?';
      for (let [key, value] of Object.entries(data)) {
        url = url + key + '=' + value + '&';
      }
      if (url.substr(url.length - 1) == '&') {
        url = url.slice(0, -1);
      }
    }
    let httpOptions = {
      headers: {
        'Authorization': `Bearer ${this.storageService.getAuthenticationHash()}`,
      },
    };
    console.log('GET request', url, data);
    return this.angularHttp.get(url, httpOptions).toPromise();
  }

  public put(url: string, body: object): Promise<any> {
    let httpOptions = {
      headers: {
        'Authorization': `Bearer ${this.storageService.getAuthenticationHash()}`,
        "Content-Type": "application/json"
      },
    };
    console.log('PUT request', url, body);
    return this.angularHttp.put(url, JSON.stringify(body), httpOptions).toPromise();
  }

  public delete(url: string): Promise<any> {
    let httpOptions = {
      headers: {
        'Authorization': `Bearer ${this.storageService.getAuthenticationHash()}`,
      },
    };
    console.log('DELETE request', url);
    return this.angularHttp.delete(url, httpOptions).toPromise();
  }
}
