import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AppPage } from '../enums/app-page';

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor(
    private router: Router,
  ) { }

  goToPage(appPage: AppPage): Promise<boolean> {
    return this.router.navigateByUrl(appPage);
  }

  goToPageWithId(appPage: AppPage, id: String): Promise<boolean> {
    return this.router.navigateByUrl(appPage + "/" + id);
  }
}
