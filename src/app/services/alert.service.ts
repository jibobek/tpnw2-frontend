import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private alertCtrl: AlertController,) { }


  public async show(title: string, text: string, popupbuttons: Array<any> = ['OK']) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: text,
      buttons: popupbuttons
    });
    await alert.present();
  }
}
