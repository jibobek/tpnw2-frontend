import { Injectable } from '@angular/core';
import { AppConfiguration } from '../const/config';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class EndpointService {

  constructor(
    private httpService: HttpService,
  ) { }

  public login(email: String, password: String): Promise<any> {
    return this.httpService.post(AppConfiguration.API_URL + "/login/", { email: email, password: password }, false);
  }

  public register(email: String, password: String): Promise<any> {
    return this.httpService.post(AppConfiguration.API_URL + "/register/", { email: email, password: password }, false);
  }

  public getPublicRecipes(page: Number, searchQuery: String): Promise<any> {
    return this.httpService.get(AppConfiguration.API_URL + "/recipes/", { page: page, search: searchQuery });
  }

  public getPublicRecipeList(page: Number, searchQuery: String): Promise<any> {
    return this.httpService.get(AppConfiguration.API_URL + "/recipes/list/", { page: page, search: searchQuery });
  }

  public getMyRecipes(page: Number): Promise<any> {
    return this.httpService.get(AppConfiguration.API_URL + "/myrecipes/", { page: page });
  }

  public getRecipe(id: String): Promise<any> {
    return this.httpService.get(AppConfiguration.API_URL + "/publicrecipe/" + id);
  }

  public updateRecipe(id: String, data: Object): Promise<any> {
    return this.httpService.put(AppConfiguration.API_URL + "/recipe/" + id, data);
  }

  public createRecipe(data: Object): Promise<any> {
    return this.httpService.post(AppConfiguration.API_URL + "/recipe/", data);
  }

  public deleteRecipe(id: String): Promise<any> {
    return this.httpService.delete(AppConfiguration.API_URL + "/recipe/" + id);
  }

  public createRating(recipeId: String, rating: Number): Promise<any> {
    return this.httpService.post(AppConfiguration.API_URL + "/rating/" + recipeId, { rating: rating });
  }

  public createComment(recipeId: String, text: String): Promise<any> {
    return this.httpService.post(AppConfiguration.API_URL + "/comment/" + recipeId, { text: text });
  }

  public getComments(recipeId: String): Promise<any> {
    return this.httpService.get(AppConfiguration.API_URL + "/publiccomments/" + recipeId);
  }
}
