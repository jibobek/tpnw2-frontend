import { Component } from '@angular/core';
import { StorageService } from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private storageService: StorageService,
  ) {
    this.loadStorageData();
  }

  /**
   * Load data from storage on any page load
   * @returns Promise
   */
  private async loadStorageData(): Promise<void> {
    await this.storageService.initStorage();
    await this.storageService.loadUserFromStorage();
    return;
  }
}
