import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StarRatingComponent } from './star-rating.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [StarRatingComponent],
    exports: [StarRatingComponent]
})
export class StarRatingComponentModule { }
