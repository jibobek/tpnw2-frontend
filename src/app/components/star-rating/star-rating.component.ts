import { Component, Input, OnInit } from '@angular/core';
import { StarState } from 'src/app/enums/star-state.enum';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
})
export class StarRatingComponent implements OnInit {
  @Input() number: Number = 0;
  @Input() color: String = "warning";
  @Input() size: String= "large";

  starStates = {
    1: StarState.FULL,
    2: StarState.FULL,
    3: StarState.FULL,
    4: StarState.FULL,
    5: StarState.FULL
  };

  constructor() { }

  ngOnInit() {
    this.updateStarStates();
  }
  ngOnChanges(){
    this.updateStarStates();
  }

  /**
   * Fill stars states by input number
   */
  private updateStarStates(): void {
    for (let i = 1; i <= 5; i++) {
      if (this.number < i - 0.5) {
        this.starStates[i] = StarState.EMPTY;
      } else if (this.number < i) {
        this.starStates[i] = StarState.HALF;
      }
    }
  }
}
