import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecipeListComponent } from './recipe-list.component';
import { RecipeCardComponentModule } from '../recipe-card/recipe-card.module';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, RecipeCardComponentModule],
    declarations: [RecipeListComponent],
    exports: [RecipeListComponent]
})
export class RecipeListComponentModule { }
