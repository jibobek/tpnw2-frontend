import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RecipeCardData } from 'src/app/types/recipe-card-data';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss'],
})
export class RecipeListComponent implements OnInit {
  @Input() recipeList:Array<RecipeCardData> = [];
  @Output() clickedEvent = new EventEmitter<String>();

  constructor() { }

  ngOnInit() {}

  clicked(id: String){
    this.clickedEvent.emit(id);
  }

}
