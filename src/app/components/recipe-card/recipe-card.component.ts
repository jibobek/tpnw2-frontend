import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-recipe-card',
  templateUrl: './recipe-card.component.html',
  styleUrls: ['./recipe-card.component.scss'],
})
export class RecipeCardComponent implements OnInit {
  @Input() id: String = "";
  @Input() name: String = "";
  @Input() stars: Number = 0;
  @Input() isPrivate: boolean = false;
  @Input() imageSrc: String = "";
  @Output() clickedEvent = new EventEmitter<String>();

  constructor() { }

  ngOnInit() { }

  click() {
    this.clickedEvent.emit(this.id);
  }
}
