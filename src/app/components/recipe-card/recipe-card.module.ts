import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecipeCardComponent } from './recipe-card.component';
import { StarRatingComponentModule } from '../star-rating/star-rating.module';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, StarRatingComponentModule],
    declarations: [RecipeCardComponent],
    exports: [RecipeCardComponent]
})
export class RecipeCardComponentModule { }
