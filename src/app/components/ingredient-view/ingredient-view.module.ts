import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IngredientViewComponent } from './ingredient-view.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [IngredientViewComponent],
    exports: [IngredientViewComponent]
})
export class IngredientViewComponentModule { }
