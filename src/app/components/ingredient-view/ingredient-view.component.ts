import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ingredient-view',
  templateUrl: './ingredient-view.component.html',
  styleUrls: ['./ingredient-view.component.scss'],
})
export class IngredientViewComponent implements OnInit {
  @Input() quantity: Number = 0;
  @Input() unit: String = "";
  @Input() name: String = "";

  constructor() { }

  ngOnInit() { }

}
