import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent implements OnInit {
  @Input() date: String = "";
  @Input() author: String = "";
  @Input() text: String = "";

  constructor() { }

  ngOnInit() { }

}
