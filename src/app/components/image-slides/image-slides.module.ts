import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImageSlidesComponent } from './image-slides.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [ImageSlidesComponent],
    exports: [ImageSlidesComponent]
})
export class ImageSlidesComponentModule { }
