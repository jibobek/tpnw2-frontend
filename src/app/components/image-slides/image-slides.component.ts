import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-slides',
  templateUrl: './image-slides.component.html',
  styleUrls: ['./image-slides.component.scss'],
})
export class ImageSlidesComponent implements OnInit {
  @Input() imagesSrc: Array<String> = [];

  constructor(
  ) { }

  ngOnInit() {
  }

}
