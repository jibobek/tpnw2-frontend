import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-step',
  templateUrl: './recipe-step.component.html',
  styleUrls: ['./recipe-step.component.scss'],
})
export class RecipeStepComponent implements OnInit {
  @Input() number: Number = 0;
  @Input() text: String = "";

  constructor() { }

  ngOnInit() { }

}
