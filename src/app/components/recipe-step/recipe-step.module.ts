import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecipeStepComponent } from './recipe-step.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [RecipeStepComponent],
    exports: [RecipeStepComponent]
})
export class RecipeStepComponentModule { }
